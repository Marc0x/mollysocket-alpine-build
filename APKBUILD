# Contributor: Marco Loletti <marco.loletti@zohomail.eu>
# Maintainer: Marco Loletti <marco.loletti@zohomail.eu>
pkgname=mollysocket
pkgver=0.2.1
pkgrel=0
pkgdesc="MollySocket allows getting signal notifications via UnifiedPush"
url="https://github.com/MollySocket/mollysocket"
arch="all"
license="AGPL-3.0-or-later"
depends="openssl sqlite"
makedepends="openssl-dev sqlite-dev rustup"
install="$pkgname.pre-install"
subpackages="$pkgname-openrc"
source="$pkgname-$pkgver.tar.gz::https://github.com/MollySocket/mollysocket/archive/refs/tags/$pkgver.tar.gz
		$pkgname.initd
		$pkgname.confd
		$pkgname
		default-config.toml
		"
builddir="$srcdir/$pkgname-$pkgver"
options="net !check"
# no check for now

export PKG_CONFIG_ALLOW_CROSS=1
export OPENSSL_STATIC=true
export RUSTFLAGS="-Ctarget-feature=-crt-static"

build() {
	rustup-init -y
	source "$HOME/.cargo/env"
	rustup install nightly
	cargo +nightly build --release
}

package() {
	install -m644 -D "$srcdir"/default-config.toml \
		"$pkgdir"/etc/$pkgname/default-config.toml
	install -m755 -D "$srcdir"/$pkgname.initd \
		"$pkgdir"/etc/init.d/$pkgname
	install -m644 -D "$srcdir"/$pkgname.confd \
		"$pkgdir"/etc/conf.d/$pkgname
	install -Dm755 "$builddir"/target/release/"$pkgname" \
		"$pkgdir"/usr/bin/ms
	install -Dm755 "$srcdir"/"$pkgname" \
		"$pkgdir"/usr/bin/"$pkgname"
}

sha512sums="
8eed75514fa628f03b12b6cc7349db83cbc58c8d7fd8c595d31e768ebc4123dc1179f732f1f819c70f7b17ba5a2fc01f8344146f4951f256f2ab6c372dcf41b6  mollysocket-0.2.1.tar.gz
f5d567cee3c34c400ef60b15e42bfda9694898c4530f6b483016c29e6e77e1640044448544581ab0833ee4ad22db0f907280814f0e9e1ddb89e7fc6a4a736dff  mollysocket.initd
9e8aa20c534ccf480699ec964ae70f9ff5b54fdc5bc1ffa4fd889eab2eb0bfaae61185aefeb0596cc32a723a939645bb6a61e30edbbe652fd0683647918f8dd6  mollysocket.confd
186215cc9662f5d6c81ce0618deecd892cb237c03a197a5c302684d98ce6389eb75d9c5ab9612a7133948700aa0f3391b9441bcd29b65dd00ec87aaa4957dd3d  mollysocket
cb8318a97e01b0f8349f5c3614e83ec1590587fcf1677e3ba33b78e4a9648f6774168f00e2bd456df6f13545b89f29a99c86042fcb2e5555008054524508c7e3  default-config.toml
"
